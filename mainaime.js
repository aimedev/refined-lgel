﻿/* Script par Aimedev */

var autoguide = ''; var autoguidetlm = 'false'; var autoyoutube = 'false';
var compoleft = 'false'; var highlight = 'false'; var blocnotes = 'false'; var afkselective = 'false';
var blacklist = ''; var blblockmessages = 'false';
var invitefriends = 'false'; var sendatroom = 'false';
var pnclip = 'false'; var pnclip1; var pnclip2; var pnclip3; var pnclip4;

var afkselective_entrees = 'none'; var afkselective_enabled = 'false';

function onGot(item) {
	try {
		autoguide = item.setts.autoguide; autoguide = autoguide.split(';');
		autoguidetlm = item.setts.autoguidetlm; compoleft = item.setts.compoleft;
		pnclip = item.setts.pnclip; pnclip1 = item.setts.pnclip1; pnclip2 = item.setts.pnclip2; pnclip3 = item.setts.pnclip3; pnclip4 = item.setts.pnclip4;
		highlight = item.setts.highlight; invitefriends = item.setts.invitefriends;
		sendatroom = item.setts.sendatroom; blocnotes = item.setts.blocnotes; afkselective = item.setts.afkselective;
		blblockmessages = item.setts.blblockmessages; blacklist = item.setts.blacklist; bll = blacklist.split(';'); bll.length = bll.length - 1;
		autoyoutube = item.setts.autoyoutube;
	} catch (e) {
		document.querySelector('#block_chat').insertAdjacentHTML('afterbegin', `<div class="block_chat_alert" style="animation-duration: 15s;padding: 10px;text-align: justify;">
			Refined Lgel ne peut pas charger les paramètres personnalisés. 
			Avez-vous correctement configuré l'application pour qu'elle modifie ce que vous souhaitez sur le site ?
			<div style="text-align: right;margin-top: -10px;">
				<a href="${chrome.runtime.getURL('/options.html')}" target="_blank" class="button_secondary">Se rendre dans les paramètres</a>
			</div>
		</div>`);
	};
	if (window.location.href.indexOf('/jeu/index.php') !== -1 && document.querySelector('#erreur') == null) {
		setTimeout(configIActions, 1000); /** Config events */
		setTimeout(function() {
			function gameModified() {
				const lastMessage = document.querySelectorAll('.canal_joueurs')[document.querySelectorAll('.canal_joueurs').length - 1];
				const lastmsgperso = document.querySelectorAll('.canal_perso')[document.querySelectorAll('.canal_perso').length - 1];
				if (document.querySelector('#scroll_chat').style.display === 'none' && pnclip === 'false') {
					document.querySelector('#scroll_chat').style.display = 'block';
				};
				if (document.querySelector('#block_high_pseudo').value !== '') { sethighl('auto'); };
				if (blblockmessages === 'true') {
					if (getPhase() === 'pregame') {
						if (lastmsgperso.children[0].classList.contains('selective-join') && !lastmsgperso.children[0].classList.contains('rl-bypass')) {
							const dataPseudo = lastmsgperso.children[0].getAttribute('data-pseudo');
							if (blacklist.indexOf(dataPseudo) !== -1) {
								lastmsgperso.children[0].querySelector('.button_secondary.negative').click();
								document.querySelector('#block_chat_game').insertAdjacentHTML('beforeend', `<div class="canal_meneur"><img class="msg-icon" style="width:18px;height:auto" src="/stuff/salle_de_jeu/partie_selective.png"> <span style="color:#606060;">[Paramètres Refined Lgel] <b>${dataPseudo}</b> fait partie de votre liste rouge.</span></div>`);
							};
							lastmsgperso.children[0].classList.add('rl-bypass');
						};
					};
				};
				if (getPhase() === 'pregame') {
					if (afkselective_enabled == 'true' && afkselective == 'true') {
						if (afkselective_entrees == 'all' && lastmsgperso.children[0].classList.contains('selective-join')
							 && !lastmsgperso.children[0].classList.contains('rl-auto-bypass')) {
							lastmsgperso.children[0].querySelector('.button_secondary.positive').click();
							lastmsgperso.children[0].classList.add('rl-auto-bypass');
						} else if (afkselective_entrees == 'none' && lastmsgperso.children[0].classList.contains('selective-join')
							 && !lastmsgperso.children[0].classList.contains('rl-auto-bypass')) {
							lastmsgperso.children[0].querySelector('.button_secondary.negative').click();
							lastmsgperso.children[0].classList.add('rl-auto-bypass');
						} else if (afkselective_entrees == 'conf' && lastmsgperso.children[0].classList.contains('selective-join')
							 && !lastmsgperso.children[0].classList.contains('rl-auto-bypass')) {
							if (lastmsgperso.children[0].innerText.indexOf('moins de 50 parties') == -1) {
								lastmsgperso.children[0].querySelector('.button_secondary.positive').click();
								lastmsgperso.children[0].classList.add('rl-auto-bypass');
							} else {
								lastmsgperso.children[0].querySelector('.button_secondary.negative').click();
								lastmsgperso.children[0].classList.add('rl-auto-bypass');
							};
						};
					};
					if ((autoguide !== '' || autoguidetlm !== 'false')) {
						Fautoguide();
					};
					if (autoyoutube === 'true') {
						let linkMatches = lastMessage.innerText.match(/(https?:\/\/www.youtube.com\/[^\s]+)/g);
						if (linkMatches.length !== 0 && !lastMessage.classList.contains('youtube')) {
							lastMessage.classList.add('youtube');
							let linkYT = linkMatches[0].replace('watch?v=', 'embed/');
							lastMessage.insertAdjacentHTML('beforeend', `<iframe src="${linkYT}" class="block shadow rounded youtube"></iframe>`);
						}
					} 
				} else if (getPhase() === 'ingame') {
					if (afkselective == 'true' && document.querySelector('#block_afk_selective') != null) { byeQS(document.querySelector('#block_afk_selective')); };
					if (sendatroom === 'true' && document.querySelector('#gm_commands') === null) {	sendatroom = 'false'; };
				};
			};
			var observerGame = new MutationObserver((mutations) => { mutations.forEach(gameModified); });
			observerGame.observe(document.querySelector('#block_chat_game'), { attributes: true, subtree: true });
			setTimeout(() => {
				if (document.querySelector('#top_bar_game') !== null) {
					if (compoleft === 'true') {
						refreshCompo = setInterval(Fcompolef, 2000);
					};
					if (blacklist !== '') {
						setInterval(Fblacklist, 5000);
						Fblacklist();
					};
				};
			}, 650);
			if (getPhase() === 'pregame') {
				if (sendatroom === 'true' && isCrea()) {
					function sendMentionRoom() {
						const blockMessage = document.querySelector('#block_chat_message');
						if (blockMessage.value.indexOf('@room') !== -1 && getPhase() === 'pregame') {
							const allplayers = document.querySelector('.list_players').querySelectorAll('.player');
							blockMessage.value = blockMessage.value.replace('@room', '');
							[].forEach.call(allplayers, (ap) => {
								blockMessage.value += `@${ap.textContent} `;
							});
							blockMessage.focus();
						} else if (getPhase() === 'ingame') {
							document.querySelector('#block_chat_message').removeEventListener('keyup', sendMentionRoom);
							sendatroom === 'false';
						};
					};
					document.querySelector('#block_chat_message').addEventListener('keyup', sendMentionRoom);
				} else {
					sendatroom = 'false';
				};
				if (afkselective === 'true' && document.querySelector('#block_afk_selective') == null 
				&& document.querySelector('.game-rules').innerText.indexOf('Seuls les joueurs autorisés par le créateur') != -1 && isCrea()) {
					document.head.insertAdjacentHTML('beforeend', `<style type="text/css">
					#block_afk_selective {margin-bottom: 7px;flex: 0 0 auto;display: grid;grid-template-columns: max-content max-content auto;}
					#block_afk_selective div:nth-of-type(1) {padding: 14px 14px 14px 28px;}
					#block_afk_selective div:nth-of-type(2) {font-size: 14px;font-weight: 700;padding: 14px;}
					#block_afk_selective div:nth-of-type(3) {padding: 10px;text-align: center;}
					#block_afk_selective div:nth-of-type(4) {grid-column: 1 / 3 span;text-align: center;}
					#block_afk_selective .type_afk {margin: 0 0; min-height: 30px; padding: 5px 10px; border-top: 1px solid rgba(255,255,255,.2); background-color: rgba(255,255,255,.1); text-align: center; color: #fff; text-decoration: none; font-family: inherit; cursor: pointer; display: inline-block;width: 100%;}
					#block_afk_selective .type_afk:hover {background-color: rgba(255,255,255,.2);}</style>`);
					document.querySelector('.col-3').insertAdjacentHTML('afterbegin', `<div id="block_afk_selective" class="rounded noise bgdarkred">
						<div><img src="/stuff/salle_de_jeu/partie_selective.png?6"></div><div>Partie sélective</div>
						<div><a class="button_secondary" id="setafkselective">Me mettre en AFK</a></div></div>`);
					try { document.querySelector('.col-3').prepend(document.querySelector('#block_ami')); } catch (e) {};
					document.querySelector('#setafkselective').addEventListener('click', () => {
						document.querySelector('#block_afk_selective').insertAdjacentHTML('beforeend', `<div>
						  <a id="afk_select_all" class="type_afk">Accepter toutes les demandes d'entrée</a>
						  <br><a id="afk_select_conf" class="type_afk">Accepter les demandes des joueurs<br>ayant joué au moins 50 parties</a>
						  <br><a id="afk_select_none" class="type_afk">Refuser toutes les demandes d'entrée</a></div>`);
						byeQS(document.querySelector('#setafkselective'));
						document.querySelectorAll('.type_afk')[0].addEventListener('click', () => {
							afkselective_entrees = 'all'; afkselective_enabled = 'true';
							document.querySelector('#block_afk_selective div:nth-of-type(4)').innerHTML = `<div style="padding:0 10px 10px;max-width: 100%;">Vous êtes actuellement <b>absent du jeu</b>.
							<br>Les demandes sont auto. <b>acceptées</b>.
							<br><small>Veuillez recharger la page pour quitter ce mode.</small></div>`;
							document.querySelector('#block_chat_game').insertAdjacentHTML('beforeend', `<div class="canal_meneur"><img class="msg-icon" style="width:18px;height:auto" src="/stuff/salle_de_jeu/partie_selective.png"> <span style="color:#606060;">[Refined Lgel] Vous avez activé la mise en AFK. Les demandes sont auto. <b>acceptées</b>.</span></div>`);
							var notRetroactive = document.querySelectorAll('.selective-join');
							[].forEach.call(notRetroactive, (nr) => { byeQS(nr.parentElement.parentElement); });
						});
						document.querySelectorAll('.type_afk')[1].addEventListener('click', () => {
							afkselective_entrees = 'conf'; afkselective_enabled = 'true';
							document.querySelector('#block_afk_selective div:nth-of-type(4)').innerHTML = `<div style="padding:0 10px 10px;max-width: 100%;">Vous êtes actuellement <b>absent du jeu</b>.
							<br>Les demandes sont auto. <b>acceptées</b> (min. 50 parties).
							<br><small>Veuillez recharger la page pour quitter ce mode.</small></div>`;
							document.querySelector('#block_chat_game').insertAdjacentHTML('beforeend', `<div class="canal_meneur"><img class="msg-icon" style="width:18px;height:auto" src="/stuff/salle_de_jeu/partie_selective.png"> <span style="color:#606060;">[Refined Lgel] Vous avez activé la mise en AFK. Les demandes sont auto. <b>acceptées</b> (min. 50 parties).</span></div>`);
							var notRetroactive = document.querySelectorAll('.selective-join');
							[].forEach.call(notRetroactive, (nr) => { byeQS(nr.parentElement.parentElement); });
						});
						document.querySelectorAll('.type_afk')[2].addEventListener('click', () => {
							afkselective_entrees = 'none'; afkselective_enabled = 'true';
							document.querySelector('#block_afk_selective div:nth-of-type(4)').innerHTML = `<div style="padding:0 10px 10px;max-width: 100%;">Vous êtes actuellement <b>absent du jeu</b>.
							<br>Les demandes sont auto. <b>refusées</b>.
							<br><small>Veuillez recharger la page pour quitter ce mode.</small></div>`;
							document.querySelector('#block_chat_game').insertAdjacentHTML('beforeend', `<div class="canal_meneur"><img class="msg-icon" style="width:18px;height:auto" src="/stuff/salle_de_jeu/partie_selective.png"> <span style="color:#606060;">[Refined Lgel] Vous avez activé la mise en AFK. Les demandes sont auto. <b>refusées</b>.</span></div>`);
							var notRetroactive = document.querySelectorAll('.selective-join');
							[].forEach.call(notRetroactive, (nr) => { byeQS(nr.parentElement.parentElement); });
						});
					});
				};
			};
		}, 1000);
	};
};
var getting = chrome.storage.local.get(['setts'], onGot);

function isBlank(str) { return (!str || /^\s*$/.test(str)); }

var refreshCompo;

function getPhase() {
	if ((document.querySelector('#block_ia') && document.querySelector('#block_chrono')) || document.querySelector('#block_fav')) {
		return 'ingame';
	} else {
		return 'pregame';
	};
};
function isEndgame() {
	if (document.querySelector('#block_fav')) {
		return 'true';
	} else {
		return 'false';
	};
};
function isCrea() {
	if (document.querySelector('#block_crea') === null) {
		return false;
	};
	return true;
};
function bye(child) {
	document.querySelector(child).parentElement.removeChild(document.querySelector(child));
};
function byeQS(child) {
	child.parentElement.removeChild(child);
};
function showmodal(title, message) {
	var body = document.querySelector('#modal_message_wrapper');
	document.querySelector('#modal_wrapper').style.width = '600px';
	body.setAttribute('class', '');	body.setAttribute('class', 'block_scrollable_wrapper scrollbar-light');
	if (title) {
		document.querySelector('#modal_message > .block_header > h3').innerHTML = title;
		document.querySelector('#modal_message > .block_header').style.display = 'block';
	} else {
		document.querySelector('#modal_message > .block_header').style.display = 'none';
	};
	body.querySelector('.block_scrollable_content').innerHTML = message;
	document.querySelector('#modal_close').style.display = 'block';
	document.querySelector('#modal').style.display = 'block';
};

function Fcompolef() {
	if (document.querySelector('#gm_commands') != null && (document.querySelector('#mdjmode') != null || document.querySelector('#block_players2') != null)) {
		document.querySelector('#block_chat_game').insertAdjacentHTML('beforeend', '<div class="canal_meneur"><img class="msg-icon" style="filter:invert(100%);width:18px;height:auto" src="'+chrome.runtime.getURL('/img/carte.png')+'"> <span style="color:#606060;">[Refined Lgel] Dans cette partie, la composition ne peut être déplacée auto. à gauche.</span></div>');
		document.querySelector('#scroll_chat').click();
		clearInterval(refreshCompo);
	} else {
		if (getPhase() === 'ingame') {
			clearInterval(refreshCompo);
			setTimeout(() => {
				document.querySelector('#top_bar_move_composition').click();
				document.querySelector('#block_chat_game').insertAdjacentHTML('beforeend', '<div class="canal_meneur"><img class="msg-icon" style="filter:invert(100%);width:18px;height:auto" src="'+chrome.runtime.getURL('/img/carte.png')+'"> <span style="color:#606060;">[Refined Lgel] La composition a été auto. déplacée à gauche.</span></div>');
				document.querySelector('#scroll_chat').click();
				setTimeout(() => {
					if (document.querySelector('#block_compo') == null) {
						if (document.querySelector('#block_chat_game').innerText.indexOf('La composition a été auto. déplacée à gauche.') !== -1) {
							document.getElementsByClassName('col-3')[0].insertAdjacentHTML('beforeend', '<div id="block_compo" class="block shadow noise rounded" style="display: block;margin-bottom: auto;"><div class="block_header"><h3>Composition de jeu</h3></div><div class="block_content">Chargement...</div></div>');
							document.querySelector('#block_ia').style.margin = '20px 0 10px 0';
							if (document.querySelector('#block_guide') != null) { document.getElementsByClassName('col-3')[0].appendChild(document.querySelector('#block_guide')); };
							if (document.querySelector('#block_skipvote') != null) { document.getElementsByClassName('col-3')[0].appendChild(document.querySelector('#block_skipvote')); };
						} else {
							document.getElementsByClassName('col-3')[1].insertAdjacentHTML('afterbegin', '<div id="block_compo" class="block shadow noise rounded"><div class="block_header"><h3>Composition de jeu</h3></div><div class="block_content">Chargement...</div></div>');
						};
					};
				}, 2500);
			}, 500);
		};
	};
};

function Fautoguide() {
	const cg = document.querySelectorAll('.canal_guide');
	[].forEach.call(cg, (cgg) => {
		if (autoguidetlm === 'false') {
			if (autoguide.indexOf(cgg.getElementsByTagName('b')[0].innerText) !== -1) {
				cgg.getElementsByTagName('a')[0].click();
				document.querySelector('#block_chat_game').insertAdjacentHTML('beforeend', `<div class="canal_meneur"><img class=msg-icon src=assets/images/icon-guide.png><span style=color:#408721>[Refined Lgel] La demande de guidage de ${cgg.getElementsByTagName('b')[0].innerText} a été automatiquement acceptée.</span></div>`);
				autoguide = ''; autoguidetlm = 'false';
			};
		} else {
			cgg.querySelector('a').click();
			autoguidetlm = 'false'; autoguide = '';
		};
	});
};

var psdwant; var hnbb;
function remhighl() {
	const els = document.getElementsByClassName('canal_joueurs');
	[].forEach.call(els, (el) => { el.classList.remove('highlighted'); });
};
function sethighl(hcal) {
	if (psdwant !== '' && hcal === 'but') {
		remhighl();
	};
	if (hcal === 'but') {
		psdwant = document.querySelector('#block_high_pseudo').value;
	};
	if (psdwant !== '') {
		if (hcal === 'but') {
			hnbb = 0;
		}
		const els = document.getElementsByClassName('canal_joueurs');
		[].forEach.call(els, (el) => {
			if (el.children[0].children[0].innerText.substr(0, psdwant.length) === psdwant && el.classList.contains('highlighted') === false) {
				el.classList.add('highlighted');
				hnbb += 1;
			};
		});
		document.querySelector('#block_high_set').innerHTML = `<div style="padding:0 10px; transform: scale(1.5);">${hnbb}</div>`;
	};
};

var bll; var bllchecked = '';
function Fblacklist() {
	[].forEach.call(bll, (bl) => {
		if (!isBlank(bl) && document.querySelector('#block_players').innerText.indexOf(bl) !== -1 && document.querySelector('#block_chat_game').innerText.indexOf(`Un joueur de votre liste rouge est présent dans ce salon : ${bl}.`) === -1 && bllchecked.indexOf(bl) === -1) {
			document.querySelector('#block_chat_game').insertAdjacentHTML('beforeend', `<div class="canal_meneur blacklisted"><b>[Refined Lgel]</b> Un joueur de votre liste rouge est présent dans ce salon : <b>${bl}</b>.</div>`);
			document.querySelector('#scroll_chat').click();
			bllchecked += `${bl};`;
		};
	});
};

function configIActions() {
	if (document.querySelector('#top_bar_game') != null && document.querySelector('#top_bar_game').innerText.indexOf('Partie') > -1 && document.querySelector('#erreur') == null) {
		try {
			if (document.querySelector('#top_bar_buttons_show') == null) {
				document.querySelector('#top_bar_buttons').insertAdjacentHTML('beforeend', '<div class="top_bar_button sound-tick" id="top_bar_buttons_show" data-tooltip="Afficher davantage de boutons"><img src="https://i.ibb.co/Km2GKsB/plus.png"></div>');
				document.querySelector('#top_bar_buttons').appendChild(document.querySelector('#top_bar_buttons_quit'));
				document.querySelector('#top_bar_buttons_show').addEventListener('click', () => {
					document.querySelector('#top_bar_buttons_refresh').style.display = 'block';
					document.querySelector('#top_bar_buttons_clear').style.display = 'block';
					document.querySelector('#top_bar_buttons_bugreport').style.display = 'block';
					document.querySelector('#top_bar_buttons_params').style.display = 'block';
					byeQS(document.querySelector('#top_bar_buttons_show'));
				});
				document.querySelector('#top_bar_buttons_refresh').style.display = 'none';
				document.querySelector('#top_bar_buttons_clear').style.display = 'none';
				document.querySelector('#top_bar_buttons_bugreport').style.display = 'none';
				document.querySelector('#top_bar_buttons_params').style.display = 'none';
			};
		} catch (e) {};
		if (document.querySelector('#top_bar_move_composition') === null) {
			document.querySelector('#top_bar_buttons').insertAdjacentHTML('afterbegin', '<div class="rl_button sound-tick" data-tooltip="Déplacer la composition de droite à gauche,<br>et inversement"><img id="top_bar_move_composition" alt="Composition" src="'+chrome.runtime.getURL('/img/carte.png')+'"></div>');
			document.querySelector('#top_bar_move_composition').parentElement.addEventListener('click', () => {
				if (document.querySelector('#block_compo') == null) {
					document.getElementsByClassName('col-3')[1].insertAdjacentHTML('afterbegin', '<div id="block_compo" class="block shadow noise rounded"><div class="block_header"><h3>Composition de jeu</h3></div><div class="block_content">Chargement...</div></div>');
				} else {
					if (document.querySelector('#block_compo').parentElement === document.getElementsByClassName('col-3')[1]) {
						try { document.querySelector('#block_ia').setAttribute('style', 'margin-bottom:10px;display:block;');
						} catch (e) { document.querySelector('#block_crea').setAttribute('style', 'margin-bottom:10px;display:block;'); };
						document.querySelector('#block_compo').setAttribute('style', 'display:block;margin-bottom:auto');
						document.getElementsByClassName('col-3')[0].setAttribute('style', 'overflow-y:auto');
						if (document.querySelector('#block_guide') != null) {
							document.querySelector('#block_actions').insertBefore(document.querySelector('#block_compo'), document.querySelector('#block_guide'));
							document.querySelector('#block_guide').style.marginTop = 'auto';
						} else if (document.querySelector('#block_skipvote') != null) {
							document.querySelector('#block_actions').insertBefore(document.querySelector('#block_compo'), document.querySelector('#block_skipvote'));
						} else {
							document.querySelector('#block_actions').appendChild(document.querySelector('#block_compo'));
						};
					} else {
						document.getElementsByClassName('col-3')[1].prepend(document.querySelector('#block_compo'));
						document.querySelector('#block_compo').style.marginBottom = '10px';
						document.querySelector('#block_compo').style.marginTop = '0px';
					};
				};
			});
		};
		if (document.querySelector('#block_high') === null) {
			document.querySelector('#top_bar_buttons').insertAdjacentHTML('afterbegin', '<div class="rl_button sound-tick" data-tooltip="Afficher / Masquer le bloc de surlignage"><img id="top_bar_show_highlight" alt="Surligneur" src="'+chrome.runtime.getURL('/img/highlight.png')+'"></div>');
			document.getElementsByClassName('col-3')[1].insertAdjacentHTML('beforeend', '<div class="block shadow noise rounded" id="block_high"><input id="block_high_pseudo" list="block_high_pseudos" placeholder="Surligner les paroles de..." type="text"><datalist id="block_high_pseudos"></datalist><span id="block_high_set">Choisir 🖐</span><span id="block_high_clear" style="display: none;">Effacer</span></div>');
			function getHoveredPseudoInTchat() {
				var n = document.querySelector(':hover');
				var nn;
				while (n) {
					nn = n;
					n = nn.querySelector(':hover');
				};
				if (nn.classList.contains('canal_joueurs') === true) {
					document.querySelector('#block_chat_game').removeAttribute('style');
					document.querySelector('#block_high_pseudo').value = nn.querySelector('.chat-pseudo').innerText;
					document.querySelector('#block_high_set').click();
					document.querySelector('#block_high_clear').style.display = '';
					document.querySelector('#block_chat_game').removeEventListener('click', getHoveredPseudoInTchat);
				};
			};
			document.querySelector('#block_high_set').addEventListener('click', () => {
				if (document.querySelector('#block_chat_game').style.cursor !== 'grab') {
					if (document.querySelector('#block_high_pseudo').value !== '') {
						sethighl('but');
					} else {
						document.querySelector('#block_chat').insertAdjacentHTML('afterbegin', '<div class="block_chat_alert">🖐 Choisissez dans le tchat de jeu les paroles d\'un joueur pour les surligner.</div>');
						document.querySelector('#block_chat_game').setAttribute('style', 'cursor: grab !important');
						document.querySelector('#block_chat_game').addEventListener('click', getHoveredPseudoInTchat);
					};
				} else {
					document.querySelector('#block_chat_game').style.cursor = 'default';
					document.querySelector('#block_chat_game').removeEventListener('click', getHoveredPseudoInTchat);
				};
			});
			document.querySelector('#block_high_clear').onclick = () => {
				if (psdwant !== '') {
					psdwant = '';
					hnbb = '';
					remhighl();
					document.querySelector('#block_high_pseudo').value = '';
					getHighStatus();
				};
			};
			document.querySelector('#top_bar_show_highlight').parentElement.onclick = () => {
				if (document.querySelector('#block_high').style.display === 'grid') {
					document.querySelector('#block_high').style.display = 'none';
					document.querySelector('#top_bar_show_highlight').setAttribute('style', '');
				} else {
					document.querySelector('#block_high').style.display = 'grid';
					document.querySelector('#top_bar_show_highlight').setAttribute('style', 'filter: drop-shadow(0 2px 2px #000) contrast(50%) sepia(100%) saturate(100%);');
				};
			};
			const dataPseud = setInterval(() => {
				if (getPhase() == 'ingame') {
					const pseuds = document.getElementsByClassName('player');
					[].forEach.call(pseuds, (pse) => {
						if (pse.innerText.indexOf('anonyme') == -1) {
							document.querySelector('#block_high_pseudos').insertAdjacentHTML('beforeend', `<option value="${pse.innerText}"></option>`);
						};
					});
					clearInterval(dataPseud);
				};
			}, 4000);
			document.querySelector('#block_high_pseudo').oninput = () => {
				if (document.querySelector('#block_high_pseudos').innerHTML.indexOf(`"${document.querySelector('#block_high_pseudo').value}"`) !== -1) {
					document.querySelector('#block_high_set').click();
				} else if (document.querySelector('#block_high_pseudo').value === '') {
					document.querySelector('#block_high_clear').click();
				};
				getHighStatus();
			};
			function getHighStatus() {
				if (document.querySelector('#block_high_pseudo').value != '') {
					document.querySelector('#block_high_clear').style.display = '';
					document.querySelector('#block_high_set').innerHTML = 'Surligner';
				} else {
					document.querySelector('#block_high_clear').style.display = 'none';
					document.querySelector('#block_high_set').innerHTML = 'Choisir 🖐';
				};
			};
			if (highlight === 'true') { document.querySelector('#top_bar_show_highlight').click(); };
		};
		if (document.querySelector('#block_clip') === null) {
			function setbtnpn(entree, nbchar) {
				const r1 = document.createElement('a');
				r1.setAttribute('class', 'rlpastebtn buttsy');
				r1.addEventListener('click', () => {
					document.querySelector('#block_chat_message').value += entree;
					document.querySelector('#block_chat_message').focus();
				});
				r1.innerText = `${entree.substr(0, nbchar)}...`;
				document.querySelector('#pclipcont').prepend(r1);
				document.querySelector('#scroll_chat').style.display = 'block';
				document.querySelector('#scroll_chat').style.display = 'none';
			};
			document.querySelector('#top_bar_buttons').insertAdjacentHTML('afterbegin', '<div class="rl_button sound-tick" data-tooltip="Afficher / Masquer le presse-papiers intégré"><img id="top_bar_show_clipboard" alt="Presse-papiers" src="'+chrome.runtime.getURL('/img/clipboard.png')+'"></div>');
			var pncolor = '#795245';
			const possibles = ['rgb(131, 95, 170)', 'rgb(95, 130, 170)', 'rgb(113, 170, 95)', 'rgb(170, 95, 95)'];
			if (document.querySelector('#top_bar_game').innerText.indexOf('Carnage') !== -1) {
				pncolor = possibles[0];
			} else if (document.querySelector('#top_bar_game').innerText.indexOf('Fun') !== -1) {
				pncolor = possibles[1];
			} else if (document.querySelector('#top_bar_game').innerText.indexOf('Normale') !== -1) {
				pncolor = possibles[2];
			} else if (document.querySelector('#top_bar_game').innerText.indexOf('Sérieuse') !== -1) {
				pncolor = possibles[3];
			};
			document.querySelector('body').insertAdjacentHTML('beforeend', `<div id="block_clip" class="noise" style="background-color: ${pncolor};">
				<div id="pclipcont" style="display: block;"></div>
				<span><span style="background: rgba(0,0,0,.1);padding: 12px;margin-left: -12px;"><img src="${chrome.runtime.getURL('/img/clipboard.png')}" style="width: 13px;margin-right: 5px;">Presse-papiers</span></span>
				<a class="buttsy" id="pclipmove">➡</a>
				<a class="buttsy" id="pclipclear">Effacer</a>
				<a class="buttsy" id="pcliphide">Plier / Déplier</a>
			</div>`);
			function getClipFromSettings() {
				if (pnclip1 !== '') { setbtnpn(pnclip1, 30); };
				if (pnclip2 !== '') { setbtnpn(pnclip2, 30); };
				if (pnclip3 !== '') { setbtnpn(pnclip3, 30); };
				if (pnclip4 !== '') { setbtnpn(pnclip4, 30); };
			};
			getClipFromSettings();
			document.getElementById('top_bar_show_clipboard').parentElement.addEventListener("click", () => {
				if (document.querySelector('#block_clip').classList.contains('visible')) {
					document.querySelector('#block_clip').classList.remove('visible');
					document.getElementById("top_bar_show_clipboard").setAttribute('style', '');
				} else {
					document.querySelector('#block_clip').classList.add('visible');
					document.getElementById("top_bar_show_clipboard").setAttribute('style', 'filter: drop-shadow(0 2px 2px #000) contrast(50%) sepia(100%) saturate(100%);');
				};
			});
			if (pnclip === "true") {
				document.getElementById('top_bar_show_clipboard').click();
			};
			document.querySelector('#scroll_chat').style.display = 'none';
			document.querySelector('#pclipmove').addEventListener('click', (e) => {
				if (document.querySelector('#block_clip').classList.contains('right')) {
					document.querySelector('#block_clip').classList.remove('right');
					e.currentTarget.innerText = '➡';
				} else {
					document.querySelector('#block_clip').classList.add('right');
					e.currentTarget.innerText = '⬅';
				}
			});
			document.querySelector('#pclipclear').addEventListener('click', () => {
				document.querySelector('#pclipcont').innerHTML = '';
				getClipFromSettings();
			});
			document.querySelector('#pcliphide').addEventListener('click', () => {
				if (document.querySelector('#pclipcont').style.display === 'block') {
					document.querySelector('#pclipcont').style.display = 'none';
				} else {
					document.querySelector('#pclipcont').style.display = 'block';					
				};
			});
			document.querySelector('#block_chat').addEventListener('mouseup', () => {
				const selObj = window.getSelection().toString();
				if (selObj !== '') {
					setbtnpn(selObj, 45);
				};
			});
		};
		if (document.querySelector('#block_notes') === null && blocnotes === 'true') {
			document.querySelectorAll('.col-3')[1].insertAdjacentHTML('afterbegin', `<div class="block rounded noise bglightblue spoiler" id="block_notes">
				<div class="backg"></div>
				<div class="block_header spoiler_button clickable" style="cursor:pointer;z-index:1;">
					<h3>Pense-bête&nbsp;<span class="spoiler_state">▼</span></h3>
				</div>
				<textarea id="notes" style="display:none;" class="inner" placeholder="Personne ne saura ce que vous écrivez ici. Les notes ne sont pas enregistrées et seront perdues si vous rafraîchissez la page."></textarea>
			</div>`);
			document.querySelectorAll('.col-3')[1].prepend(document.querySelector('#block_compo'));
			document.querySelector('#block_notes .spoiler_button').onclick = (e) => {
				e.preventDefault();
				const inner = document.querySelector('#block_notes').querySelector('.inner');
				const sstate = document.querySelector('#block_notes').querySelector('.spoiler_state');
				if (inner.style.display === 'none') {
					inner.style.display = '';
					sstate.innerHTML = '✕';
				} else {
					inner.style.display = 'none';
					sstate.innerHTML = '▼';
				};
			};
		};
		if (document.querySelector('#invite_all_friends') === null && document.querySelector('#block_ami') != null && invitefriends === 'true') {
			document.querySelector('#block_ami').children[1].insertAdjacentHTML('beforeend', '<section data-tooltip="Envoyer une invitation à mes amis connectés" class="button_secondary sound-tick" id="invite_all_friends" style="box-shadow: 0 2px 2px rgba(0,0,0,.2);"><img alt="Amis" style="filter:brightness(2);width: 14px;margin-right: 6px;height: 14px;margin-top: 4px;" src="/static/img/friends/icon-people.png">Inviter mes amis connectés</section>');
			function InviteMyFriends() {
				const buttons = document.getElementsByClassName('invite_options');
				if (buttons.length > 0) {
					for (var i = 0; i < buttons.length; i++) {
						buttons[i].click();
					};
					document.querySelector('#block_chat_game').insertAdjacentHTML('beforeend', `<div id="chkd" class="canal_meneur"><img class="msg-icon icone" style="filter:invert(1)" src="/static/img/friends/icon-people.png">Refined Lgel a envoyé une invitation à <b>${buttons.length}</b> ami(s) connecté(s).</div>`);
					document.querySelector('#scroll_chat').click();
					byeQS(document.querySelector('#invite_all_friends'));
				};
			};
			document.querySelector('#invite_all_friends').addEventListener('click', InviteMyFriends);
		};
	};
};
