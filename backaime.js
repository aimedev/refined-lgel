﻿/* Script par Aimedev */

var fastgame = 'fastoff'; var quests = 'notmove';
var partiespe = 'none'; var partiepsn = 'boxson'; var partiepsnmode = 'none';
var tchatpremium = 'false'; var roomsfilters = 'false';

function onGot(item) {
	try {
		tchatpremium = item.room.tchatpremium; partiespe = item.room.partiespe;
		partiepsn = item.room.partiepsn; partiepsnmode = item.room.partiepsnmode;
		fastgame = item.room.fastgame; quests = item.room.quests;
		roomsfilters = item.room.roomsfilters;
	} catch (e) { };
	modifiRest();
}

var getting = chrome.storage.local.get(['room'], onGot);


var partiespeignore = ''; var tt9 = '';
var partiepsnignore = ''; var tt0 = '';

function modifiRest() {
	if (document.querySelectorAll('.link-btn-jouer').length > 0 && document.querySelector('#paramrl1') == null) {
		document.querySelector('.friends-links').insertAdjacentHTML('beforeend', '<li><a id="paramrl1" href="" target="_blank"><img src="'+chrome.runtime.getURL('/img/RL96.png')+'" alt="Refined">Options Refined Lgel</a></li>');
		document.querySelector('.friends-links-small').insertAdjacentHTML('beforeend', '<li><a id="paramrl2" href="" target="_blank" data-tooltip="Options Refined Lgel"><img src="'+chrome.runtime.getURL('/img/RL96.png')+'"></a></li>');
		document.head.insertAdjacentHTML('beforeend', '<style>.friends .friends-links-small a img, .friends .friends-links-small button img {width: 16px !important;}</style>');
		document.querySelector('#paramrl1').href = chrome.runtime.getURL('/options.html'); document.querySelector('#paramrl2').href = chrome.runtime.getURL('/options.html');
		if (tchatpremium === 'true') {
			document.head.insertAdjacentHTML('beforeend', '<style>#chatWindow{display:none !important;}</style>');
		};
	};
	if (document.querySelectorAll('.header-levels').length > 0) {
		if (fastgame === 'faston') {
			document.querySelector('.infotop').parentElement.parentElement.setAttribute('style', 'display:none;');
			document.querySelector('.featured-games').setAttribute('style', 'display:none;');
		};
		if (quests === 'move') {
			document.querySelector('.room-page').appendChild(document.querySelector('.header-levels'));
			document.querySelector('.header-levels').setAttribute('style', 'margin-top:40px;');
			document.querySelector('.room-page').appendChild(document.querySelector('.levels'));
		};
	}
	if (document.querySelectorAll('.header-waiting').length > 0) {
		if (roomsfilters === 'true') {
			let filters = document.createElement('div');
			filters.innerHTML = `<div class="choose-filter">
				<div style="padding: 5px;">N'afficher que les parties comportant l'option : </div>
				<a class="button_secondary apply-filter" data-filter="no">(Tout afficher)</a>
				<a class="button_secondary apply-filter" data-filter="MDJ">MDJ</a>
				<a class="button_secondary apply-filter" data-filter="Anonyme">Anonyme</a>
				<a class="button_secondary apply-filter" data-filter="Vocale">Vocale</a>
				<a class="button_secondary apply-filter" data-filter="Partie blanche">Sans points</a>
				<a class="button_secondary apply-filter" data-filter="Partie sélective">Sélective</a>
			</div><div class="filter"></div><div class="center filter-state" style="margin-top: 10px;"></div>`;
			filters.setAttribute('style', 'display: block;');
			filters.setAttribute('class', 'block bgbrown noise rounded');
			filters.setAttribute('id', 'refined-rooms-filters');
			document.querySelector('.room-page').insertBefore(filters, document.querySelector('.room-page .header-waiting'));
			let filtersButtons = document.querySelectorAll('#refined-rooms-filters .choose-filter a.button_secondary');
			[].forEach.call(filtersButtons, (fButton) => {
				fButton.addEventListener('click', (e) => {
					let chosenFilter = e.currentTarget.getAttribute('data-filter') || '';
					if (chosenFilter === 'no') {
						document.querySelector('#refined-rooms-filters .filter').innerHTML = '';
						document.querySelector('#refined-rooms-filters .filter-state').innerHTML = '';
					} else if (chosenFilter !== '') {
						document.querySelector('#refined-rooms-filters .filter').innerHTML = '';
						document.querySelector('#refined-rooms-filters .filter').insertAdjacentHTML('beforeend', `<style>.waiting-game:not([data-tooltip*="${chosenFilter}"]) { display: none !important; }</style>`);
						chosenFilter = chosenFilter.replace('Partie blanche', 'Sans points');
						document.querySelector('#refined-rooms-filters .filter-state').innerHTML = `Seules les parties avec l'option « ${chosenFilter} » sont affichées.`;
					}
				});
			});
		}
		if (partiespe !== 'none' || partiepsn !== '') {
			function closepartiespe() {
				document.querySelector('#modal_close').click();
				try { document.body.removeChild(document.querySelector('#partiespeson')); } catch (e) { };
			}
			const observerSpe = new MutationObserver(() => {
				if (partiespe !== 'none' && document.querySelector('#modal').style.display !== 'block') {
					const type9 = document.querySelectorAll('.type-9');
					[].forEach.call(type9, (t9) => {
						if (partiespeignore.indexOf(t9.children[2].textContent) === -1) {
							tt9 = t9.children[2].textContent; partiespeignore += `${tt9} ; `;
							if (partiespe === 'son' || partiespe === 'boxson') {
								if (document.querySelector('#partiespeson') == null) {
									document.body.insertAdjacentHTML('beforeend', '<audio id="partiespeson" src="https://www.loups-garous-en-ligne.com/static/sound/pantheon-fanfare.mp3" style="width:1px;height:1px;display:none" autoplay></audio>');
									setTimeout(() => { try { document.body.removeChild(document.querySelector('#partiespeson')); } catch (e) {} }, 10000);
								}
								document.querySelector('#modal_close').onclick = closepartiespe;
							}
							if (partiespe === 'box' || partiespe === 'boxson') {
								tt9 = t9.children[2].textContent;
								document.querySelector('#modal').classList.add('rlpartiespe'); document.querySelector('#modal').style.display = 'block';
								document.querySelector('#modal').querySelector('.block_header').innerHTML = '<h3>Refined Lgel vous informe</h3>';
								document.querySelector('#modal').querySelector('.block_scrollable_content').innerHTML = 'Une partie Spéciale a été lancée par un animateur site.<br>Souhaitez-vous la rejoindre ?<br><br><div id="partiespedtls"></div><br><small style="font-size: 0.7em;">Les boutons "Rejoindre" et "Observer" ne sont pas disponibles si vous vous trouvez déjà dans un salon.</small>';
								try {
									document.querySelector('#partiespedtls').innerHTML = '';
									document.querySelector('#partiespedtls').appendChild(t9); t9.setAttribute('id', 'partiespeshowed'); partiespeignore += tt9 + ' ; ';
									t9.children[1].setAttribute('style', 'font-size:1.3em;margin-bottom:10px;font-weight:bold');
									document.querySelector('#modal_message').setAttribute('style', '');
									t9.children[6].setAttribute('style', 'margin-top:15px'); t9.classList.remove('type-9');
									t9.children[6].children[0].setAttribute('style', 'color:#000;margin-right:15px');
									t9.children[6].children[0].onclick = closepartiespe;
								} catch (e) { }
							}
						}
					});
				}
				if (partiepsn !== '' && document.querySelector('#modal').style.display !== 'block') {
					const type0 = document.querySelectorAll('.waiting-game');
					[].forEach.call(type0, (t0) => {
						if (partiepsnignore.indexOf(t0.children[2].textContent) === -1 && t0.children[2].textContent === partiepsn) {
							tt0 = t0.children[2].textContent; partiepsnignore += `${tt0} ; `;
							if (partiepsnmode === 'son' || partiepsnmode === 'boxson') {
								if (document.querySelector('#partiespeson') == null) {
									document.body.insertAdjacentHTML('beforeend', '<audio id="partiespeson" src="https://www.loups-garous-en-ligne.com/static/sound/pantheon-fanfare.mp3" style="width:1px;height:1px;display:none" autoplay></audio>');
									setTimeout(() => { try { document.body.removeChild(document.querySelector('#partiespeson')); } catch (e) {} }, 10000);
								};
								document.querySelector('#modal_close').onclick = closepartiespe;
							};
							if (partiepsnmode === 'box' || partiepsnmode === 'boxson') {
								tt0 = t0.children[2].textContent;
								document.querySelector('#modal').classList.add('rlpartiespe'); document.querySelector('#modal').style.display = 'block';
								document.querySelector('#modal').querySelector('.block_header').innerHTML = '<h3>Refined Lgel vous informe</h3>';
								document.querySelector('#modal').querySelector('.block_scrollable_content').innerHTML = `${partiepsn} a lancé une partie.<br>Souhaitez-vous la rejoindre ?<br><br><div id="partiespedtls"></div><br><small style="font-size: 0.7em;">Les boutons "Rejoindre" et "Observer" ne sont pas disponibles si vous vous trouvez déjà dans un salon.</small>`;
								try {
									document.querySelector('#partiespedtls').innerHTML = '';
									document.querySelector('#partiespedtls').appendChild(t0); t0.setAttribute('id', 'partiespeshowed'); partiepsnignore += `${tt0} ; `;
									t0.children[1].setAttribute('style', 'font-size:1.3em;margin-bottom:10px;font-weight:bold');
									var pncolor = '#795245'; const possibles = ['rgb(131, 95, 170)', 'rgb(95, 130, 170)', 'rgb(113, 170, 95)', 'rgb(170, 95, 95)'];
									if (t0.classList.contains('type-3') === true) { pncolor = possibles[0]; };
									if (t0.classList.contains('type-1') === true) { pncolor = possibles[1]; };
									if (t0.classList.contains('type-0') === true) { pncolor = possibles[2]; };
									if (t0.classList.contains('type-2') === true) { pncolor = possibles[3]; };
									document.querySelector('#modal_message').setAttribute('style', `background-color: ${pncolor}`);
									t0.children[6].setAttribute('style', 'margin-top:15px'); t0.setAttribute('class', 'waiting-game phase-0');
									t0.children[6].children[0].setAttribute('style', 'color:#000;margin-right:15px');
									t0.children[6].children[0].onclick = closepartiespe;
								} catch (e) { };
							};
						};
					});
				};
			});
			observerSpe.observe(document.querySelector('.games-waiting'), { attributes: true, subtree: true, childList: true, characterData: true });
		};
	};
};
