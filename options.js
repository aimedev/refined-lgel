/* Script par Aimedev */

var dataimage = [];

function returnchecked(ud){	return document.getElementById(ud).checked ? "true" : "false"; }

function saveOptions(e) {
	e.preventDefault();
	dataimage.indexOf(document.querySelector('#ctmimage').value) == -1 && document.querySelector('#ctmimage').value != ''
		? dataimage.unshift(document.querySelector('#ctmimage').value) : null;
	dataimage.length > 5 ? dataimage.pop() : null;
	document.querySelector('#blacklist').value = document.querySelector('#blacklist').value.replace(new RegExp(';;', 'g'), ';');
	document.querySelector("#blacklist").value.substring(document.querySelector("#blacklist").value.length - 1) != ';' && document.querySelector('#blacklist').value != ''
		? document.querySelector("#blacklist").value += ';' : null;
	document.querySelector('#blacklist').value.indexOf('Aimedev;') != -1 ? document.querySelector('#blacklist').value = document.querySelector('#blacklist').value.replace(new RegExp('Aimedev;', 'g'), '') : null;
	document.querySelector('#autoguide').value = document.querySelector('#autoguide').value.replace(new RegExp(';;', 'g'), ';');
	document.querySelector('#autoguide').value = document.querySelector('#autoguide').value.replace(new RegExp(';;', 'g'), ';');
	document.querySelector('#autoguide').value = document.querySelector('#autoguide').value.replace(new RegExp(' ', 'g'), '');
	(document.querySelector("#autoguide").value.substring(document.querySelector("#autoguide").value.length - 1) != ';' && document.querySelector('#autoguide').value != '') ? document.querySelector("#autoguide").value += ';'	: null;
	chrome.storage.local.set({
	room: {
		fastgame: document.querySelector("#fast").value,
		quests: document.querySelector("#quests").value,
		tchatpremium : returnchecked('tchatpremium'),
		partiespe: document.querySelector("#partiespe").value,
		partiepsn: document.querySelector("#partiepsn").value,
		partiepsnmode: document.querySelector("#partiepsnmode").value,
		roomsfilters: returnchecked('roomsfilters')
	},
	theme: {
		themesite: document.querySelector("#themesite").value,
		themejeu: document.querySelector("#themejeu").value,
		autodarktheme: returnchecked('autodarktheme'),
		themecolor: document.querySelector("#ctmcolor").value,
		themeimage: document.querySelector("#ctmimage").value,
		datactmimage: dataimage
	},
	setts: {	
		blacklist: document.querySelector("#blacklist").value,
		blblockmessages: returnchecked('blblockmessages'),
		sendatroom: returnchecked('sendatroom'),
		invitefriends: returnchecked('invitefriends'),
		autoguide: document.querySelector("#autoguide").value,
		autoguidetlm: returnchecked('autoguidetlm'),
		highlight: returnchecked('highlight'),
		compoleft: returnchecked('compoleft'),
		pnclip: returnchecked('pnclip'),
		pnclip1: document.querySelector("#pnclip1").value,
		pnclip2: document.querySelector("#pnclip2").value,
		pnclip3: document.querySelector("#pnclip3").value,
		pnclip4: document.querySelector("#pnclip4").value,
		blocnotes: returnchecked('blocnotes'),
		afkselective: returnchecked('afkselective'),
		autoyoutube: returnchecked('autoyoutube')
	}
	});
	document.querySelector("#ok").style.display = "none";
	document.querySelector("#psaved").style.display = "block";
	document.querySelector("#psaved").style.opacity = "1";
}

function restoreOptions() {

  function setCurrentChoice(result) {
	try {
		document.querySelector("#themesite").value = result.theme.themesite || "notheme";
		document.querySelector("#themejeu").value = result.theme.themejeu || "notheme";
		if (result.theme.themejeu == "ctmtheme") { document.getElementById('ctmpanel').setAttribute('style', 'font-size: 15px; display: block;'); };
		if (result.theme.autodarktheme == "true") { document.getElementById('autodarktheme').checked = true; };
		document.querySelector("#ctmcolor").value = result.theme.themecolor || "";
		document.querySelector("#ctmimage").value = result.theme.themeimage || "";
		document.querySelector("#fast").value = result.room.fastgame || "fastoff";
		document.querySelector("#quests").value = result.room.quests || "notmove";
		document.querySelector("#partiespe").value = result.room.partiespe || "none";
		document.querySelector("#partiepsn").value = result.room.partiepsn || "";
		document.querySelector("#partiepsnmode").value = result.room.partiepsnmode || "box";
		if (result.room.roomsfilters == "true") { document.getElementById('roomsfilters').checked = true; };
		if (result.room.tchatpremium == "true") { document.getElementById('tchatpremium').checked = true; };
		if (result.setts.compoleft == "true") { document.getElementById('compoleft').checked = true; };
		if (result.setts.highlight == "true") { document.getElementById('highlight').checked = true; };
		if (result.setts.sendatroom == "true") { document.getElementById('sendatroom').checked = true; };
		if (result.setts.invitefriends == "true") { document.getElementById('invitefriends').checked = true; };
		document.querySelector("#autoguide").value = result.setts.autoguide || "";
		if (result.setts.autoguidetlm == "true") { document.getElementById('autoguidetlm').checked = true; };
		document.querySelector("#blacklist").value = result.setts.blacklist || "";
		if (result.setts.blblockmessages == "true") { document.getElementById('blblockmessages').checked = true; };
		if (result.setts.pnclip == "true") { document.getElementById('pnclip').checked = true; };
		if (result.setts.blocnotes == "true") { document.getElementById('blocnotes').checked = true; };
		if (result.setts.afkselective == "true") { document.getElementById('afkselective').checked = true; };
		if (result.setts.autoyoutube == "true") { document.getElementById('autoyoutube').checked = true; };
		document.querySelector("#pnclip1").value = result.setts.pnclip1 || "";
		document.querySelector("#pnclip2").value = result.setts.pnclip2 || "";
		document.querySelector("#pnclip3").value = result.setts.pnclip3 || "";
		document.querySelector("#pnclip4").value = result.setts.pnclip4 || "";
		[].forEach.call(result.theme.datactmimage, function(d) {  document.querySelector('#datactmimage').insertAdjacentHTML('beforeend', '<option value="'+d+'"></option>'); });
		dataimage = result.theme.datactmimage;
	} catch (e) {
		console.log('[Refined Lgel] Impossible de charger les paramètres : inexistants ou corrompus.');
	};
  }

  function onError(error) {
    console.log('Error: ${error}');
  }

  var getting = chrome.storage.local.get(['theme', 'room', 'setts'], setCurrentChoice);
}

document.addEventListener("DOMContentLoaded", function(){
	restoreOptions();
	var summaries = document.querySelectorAll('.summy');
	[].forEach.call(summaries, function(s) { s.onclick = function(){
		var t = this.parentElement;
		var summaries = document.querySelectorAll('.pcategory');
		[].forEach.call(summaries, function(s) { s.removeAttribute('open'); s.setAttribute('style', 'display:none');  });
		t.setAttribute('style', 'display:unset');
		document.querySelector('#paragoback').setAttribute('style', 'display:unset');
		document.querySelector('#paraminhead').setAttribute('style', 'display:none');
		document.querySelector('.toptitle').innerHTML = t.children[0].innerText;
		document.querySelector('#expl').style.display = "none";
		document.querySelector('#credits').style.display = "none";
	}; });
});
document.querySelector("form").addEventListener("submit", saveOptions);

document.querySelector('#reinit').addEventListener('click', function() {
	var prt = prompt('Vous êtes sur le point de réinitialiser tous les paramètres de Refined Lgel.\nEn faisant cela, vous remettez le module à neuf, mais vous perdez toute la configuration déjà établie dessus, comprenant les données du presse-papiers, les noms inscrits sur votre liste rouge, etc.\n\nVeuillez entrer le mot CONFIRMER afin de valider la réinitialisation.');
	if ( prt === 'CONFIRMER' ) {
		chrome.storage.local.clear();
		alert('👨‍🌾 Refined Lgel a correctement été réinitialisé.\nTous les paramètres sont revenus à leur valeur par défaut.');
		window.close();
	}
});
document.querySelector('#exportparams').addEventListener('click', function() {
	alert('🔧 Refined Lgel va vous générer un texte que vous devez conserver précieusement dans un fichier.\nVous pouvez créer celui-ci avec, par exemple, le bloc-notes intégré à Windows.\n\nCette chaîne de caractères pourra être lue par n\'importe quel navigateur disposant de Refined Lgel dans sa dernière version.\nAussi, veuillez ne pas modifier ce texte de configuration (au risque de le casser entièrement).');
	try {
		var par = 'config:RefinedLgel;';
		var inputes = document.querySelector('#para').querySelectorAll('input');
		var selectes = document.querySelector('#para').querySelectorAll('select');
		var textareas = document.querySelector('#para').querySelectorAll('textarea');
		[].forEach.call(inputes, function(s){
			try {
				if ( s.getAttribute('type') == 'checkbox' ) {
					par += s.getAttribute('id').toString() + '|' + returnchecked(s.getAttribute('id')) +'|';
				} else {
					par += s.getAttribute('id').toString() + '|' + s.value +'|';	
				}	
			} catch(e) {};
		});
		[].forEach.call(selectes, function(s){
			par += s.getAttribute('id').toString() + '|' + s.value +'|';
		});
		[].forEach.call(textareas, function(s){
			par += s.getAttribute('id').toString() + '|' + s.value +'|';
		});
		document.querySelector('#exportparamsdata').style.display = 'block';
		document.querySelector('#exportparamsdata').innerHTML = 'Voici la chaîne de caractères que vous devez conserver :<br><br>' + par.substring(0, par.length - 1);
	} catch(e) {
		alert('Erreur dans la génération du texte.');
	};
});
document.querySelector('#importparams').addEventListener('click', function() {
	var prt = prompt('🔧 Veuillez entrer le texte de configuration de Refined Lgel (celui-ci ayant été créé au préalable via le bouton « Exporter la configuration ») :\n\nRemarque : tout texte de configuration que vous aurez modifié ne sera pas reconnu par Refined Lgel.');
	if ( prt != "" && prt != null && prt != "null" ) {
		try {
			var par = prt.replace('config:RefinedLgel;', '').split('|'); var parnb = par.length / 2;
			for (i = 0; i < parnb; i++) {
				var s = document.getElementById(par[0]);
				if ( s != null ) {
					try {
						if ( s.getAttribute('type') == 'checkbox' && par[1] == 'true' ) {
							s.checked = true;
						} else {
							s.value = par[1];
						}
					} catch(e) {
					};
					par.splice(0, 2);
				};
			};
			alert('😉 Les paramètres ont été importés. Vérifiez-les puis pensez à utiliser le bouton de sauvegarde.');
		} catch(e) {
			alert('Erreur dans l\'importation des paramètres.');
		};
	};
});
document.querySelector('#reloa').addEventListener('click', function() {
	alert('👨‍🌾 Refined Lgel va être instantanément rechargé.\nIl vous est vivement conseillé de redémarrer votre session Lgel, en vous déconnectant, en fermant le navigateur, puis en vous reconnectant.');
	chrome.runtime.reload();
	window.close();
});
document.querySelector('#reportbug').addEventListener('click', function() {
	let createData = {    type: "popup", url: "https://discord.gg/cteQqmA", width:900, height:680, top:100, left:250 };
	let creating = chrome.windows.create(createData);
	window.close();
});
document.querySelector('#addbl').addEventListener('click', function() {	var prt = prompt('Entrez le pseudo du joueur à ajouter dans votre liste rouge :');
	prt != "" && prt != null && prt != "null" ? document.querySelector('#blacklist').value += prt + ';' : null;
});
document.querySelector('#removebl').addEventListener('click', function() {	var prt = prompt('Entrez le pseudo du joueur à enlever de votre liste rouge :');
	prt != "" ? document.querySelector('#blacklist').value = document.querySelector('#blacklist').value.replace(new RegExp(prt + ';', 'g'),'') : null;
});
document.querySelector('#clearbl').addEventListener('click', function() {	var prt = prompt('Entrez \'OUI\' pour confirmer l\'effacement de la liste rouge :');
	prt === "OUI" ? document.querySelector('#blacklist').value = '' : null;
});
document.querySelector('#themejeu').addEventListener('change', function() {
	document.querySelector('#themejeu').value == "ctmtheme"
		? document.querySelector('#ctmpanel').style.display = "block"
		: document.querySelector('#ctmpanel').style.display = "none";
});
document.querySelector('#cgu').addEventListener('click', function() {
	let createData = {    type: "popup", url: "https://www.gitlab.com/aimedev/refined-lgel/-/raw/master/LICENSE.md", width:900, height:680, top:100, left:250 };
	let creating = chrome.windows.create(createData);
});

const navigationHref = document.querySelectorAll('.navigation-href');
[].forEach.call(navigationHref, (nh) => {
	nh.addEventListener('click', (e) => { selectSettings(e.currentTarget.getAttribute('data-set')); });
});
function selectSettings(queri) {
	switch (queri) {
		case 'apparence':
			hideAllSettingsParents();
			const settingsApparence = document.querySelectorAll('.apparence');
			[].forEach.call(settingsApparence, (sc) => { sc.style.display = 'block'; });
			break;
		case 'social':
			hideAllSettingsParents();
			const settingsSocial = document.querySelectorAll('.social');
			[].forEach.call(settingsSocial, (sc) => { sc.style.display = 'block'; });
			break;
		case 'pregame':
			hideAllSettingsParents();
			const settingsPregame = document.querySelectorAll('.pregame');
			[].forEach.call(settingsPregame, (sc) => { sc.style.display = 'block'; });
			break;
		case 'ingame':
			hideAllSettingsParents();
			const settingsIngame = document.querySelectorAll('.ingame');
			[].forEach.call(settingsIngame, (sc) => { sc.style.display = 'block'; });
			break;
		case 'maintenance':
			hideAllSettingsParents();
			const settingsMaintenance = document.querySelectorAll('.maintenance');
			[].forEach.call(settingsMaintenance, (sc) => { sc.style.display = 'block'; });
			break;
	};
};
function hideAllSettings() {
	const settings = document.querySelectorAll('.setting');
	[].forEach.call(settings, (s) => { s.style.display = 'none'; s.parentElement.parentElement.style.display = 'none'; });
};
function hideAllSettingsParents() {
	const settings = document.querySelectorAll('.setting');
	[].forEach.call(settings, (s) => {
		if (s.classList.contains('exclude') === false) { s.style.display = 'block'; }
		s.parentElement.parentElement.style.display = 'none';
	});
};