# Refined LGeL

Simplifie l'interface de Loups-Garous en Ligne et ajoute de nouvelles fonctionnalités.
Cette extension est proposée gratuitement en open source par le joueur Aimedev.

Voir LICENSE.md pour les conditions d'utilisation.