# Conditions de L'Extension Refined Lgel

## Définition des termes
Le Développeur désigne Aimedev (https://gitlab.com/aimedev).
Le Module (ou L'Extension) désigne Refined Lgel délivré gratuitement et sans garantie de validité d'usage par le Développeur.
Les Navigateurs désignent Mozilla Firefox et Google Chrome.

## Préambule
L'Extension a été imaginée et conçue par le Développeur, à partir des idées de joueurs.
Née le vendredi 22 juin 2018, certaines de ses fonctionnalités sont inspirées de l'extension Chrome du joueur « fourtytwo », datant de décembre 2016, et retirée à ce jour.

## Données
L'Extension ne collecte aucune donnée personnelle auprès de l'utilisateur à l'exception du pseudonyme, et n'accède aucunement aux serveurs de Loups-Garous en Ligne. Tous les paramètres du module sont stockés localement, sur l'ordinateur utilisé par l'utilisateur pour se connecter à sa session Lgel, et aucune donnée de configuration n'est envoyée au Développeur.

Le Module n'agit que sur votre interface, et aucune donnée ne transite via les serveurs de Lgel : vous êtes responsables de l'utilisation que vous faites de ce Module : tout détournement ne saurait être retenu à la charge du développeur.

## Redistribution
En utilisant le Module, vous acceptez de ne pas dupliquer son code source et de le redistribuer au nom du Développeur sans son accord. La version publique de ce Module est fournie par le Développeur seul sur les magasins d'extensions des Navigateurs, et la base du code est open source pour une utilisation personnelle.

## Contribution
Le Développeur accepte que vous lui partagiez toute suggestion que vous semblez intéressante pour L'Extension. Vous pouvez également envoyer des extraits de code au Développeur (par exemple, pour une nouvelle fonctionnalité), mais ceux-ci seront au préalable retravaillés pour les rendre compatible avec la version grand public de l'Extension, et sans garantie d'être intégrés dans cette version publique du module.